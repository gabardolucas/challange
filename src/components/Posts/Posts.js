import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchPostsAction, fetchPostsSuccessAction } from './../../actions/postsActions';
import { posts } from './../../commons/Posts';

export class Posts extends Component {
	static propTypes = {
    posts: PropTypes.array.isRequired,
    filter: PropTypes.string,
  }

  static defaultProps = {
  	filter: ''
	}

	componentWillMount () {
		const { fetchPostsAction, fetchPostsSuccessAction } = this.props
		fetchPostsAction()
		
		setTimeout(() => {
			fetchPostsSuccessAction(posts)
		}, 500)
	}


	render () {
		let gridClass = 'col-xs-6'
		let postCounter = 0;		
		const { posts, filter, isLoading } = this.props
		return(
			<div>
				{isLoading && 
					<img className="post__loader" src={require('./loader.gif')} alt="Menu"/>
				}
				{posts.length > 0 && !isLoading &&
					<div className="posts row">
						{	posts.map((post, idx) => {
							const { title, image, text, author, authorImage, link, category } = post
							
							if (filter === '' || category === filter) {
								switch(postCounter) { 
								   	case 0: { 
								      gridClass = 'col-md-6' 
								      break; 
								   	}
								   	case 1: { 
								      gridClass = 'col-xs-6 col-md-3' 
								      break; 
								   	}
								   	case 2: { 
								      gridClass = 'col-xs-6 col-md-3' 
								      break; 
								   	}
								   	default: { 
								     gridClass = 'col-xs-6 col-md-4'
								    break; 
								  }
							  }							  

							  postCounter ++;

								return (
									<a href={link} key={idx} className={'post post--' + (postCounter-1) + ' ' + gridClass}>
										<div className={'post__category ' + category}>{category}</div>
										<div className="post__image"><img width="505" src={image} alt={image} /></div>
										<div className="post__title">{title}</div>
										<div className="post__author">
											<img className="post__author-image" src={authorImage} alt={author} />
											<span className="post__author-name">by {author}</span>
										</div>
										<div className="post__text">{text}</div>
									</a>									 
								)
							} else {
								return false;
							}							
						})}
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = store => ({
  isLoading: store.postsReducer.isLoading,
  posts: store.postsReducer.posts,
});

const mapDispatchToProps = { fetchPostsAction, fetchPostsSuccessAction };

export default connect(mapStateToProps, mapDispatchToProps)(Posts);

