import { GET_USER_ISLOGGED_ACTION, 
	GET_USER_ISLOGGED_SUCCESS_ACTION, 
	GET_USER_ISLOGGED_ERROR_ACTION, 
	SET_USER_PREFERENCES_ACTION,
	SET_USER_NAME_ACTION } from './actionTypes';

export const fetchUserAction = () => ({
  type: GET_USER_ISLOGGED_ACTION
});

export const fetchUserSuccessAction = () => ({
  type: GET_USER_ISLOGGED_SUCCESS_ACTION
});

export const fetchUserErrorAction = () => ({
  type: GET_USER_ISLOGGED_ERROR_ACTION
});

export const patchUserPreferencesAction = preferences => ({
  type: SET_USER_PREFERENCES_ACTION,
  preferences
});

export const patchUserNameAction = name => ({
  type: SET_USER_NAME_ACTION,
  name
});