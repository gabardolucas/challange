import { GET_USER_ISLOGGED_ACTION, 
  GET_USER_ISLOGGED_SUCCESS_ACTION, 
  GET_USER_ISLOGGED_ERROR_ACTION, 
  SET_USER_PREFERENCES_ACTION, 
  SET_USER_NAME_ACTION } from '../actions/actionTypes';

const initialState = {
  isLogged: false,
  isLoading: false,
  preferences: [],
  error: false,
  userName: '',
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_ISLOGGED_ACTION:
      return {
        ...state,
        isLoading: true,
        error: false,
      };
    case GET_USER_ISLOGGED_SUCCESS_ACTION:
      return {
        ...state,
        isLoading: false,
        isLogged: true,
      };
    case GET_USER_ISLOGGED_ERROR_ACTION:
      return {
        ...state,
        isLoading: false,
        isLogged: false,
        error: true,
      };
    case SET_USER_PREFERENCES_ACTION:
      return {
        ...state,
        preferences: action.preferences,
      };
    case SET_USER_NAME_ACTION:
      return {
        ...state,
        userName: action.name,
      };  
    default:
      return state;
  }
};